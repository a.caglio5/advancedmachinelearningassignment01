import pandas as pd
import os
from sklearn.model_selection import train_test_split
from keras import backend as K
from assignment01.functions import preprocess_data, neural_network, plot_act_func_results, risultati, \
    latitude_correction, longitude_correction

# DATA DIRECTORY
current_directory = os.getcwd()
train_folder = current_directory + "/train/"
test_folder = current_directory + "/test/"

# IMPORT DATA
X_train = pd.read_csv(train_folder + "X_train.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")
y_train = pd.read_csv(train_folder + "y_train.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")
X_test = pd.read_csv(test_folder + "X_test.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")

# PRE-PROCESS DATA
# Remove instances with price >= 1000
X_train = X_train[y_train['price'] < 1000]
y_train = y_train[y_train['price'] < 1000]

# Correct instances with abnormal latitude value
X_train['latitude'] = X_train['latitude'].apply(lambda latitude: latitude_correction(latitude))
X_test['latitude'] = X_test['latitude'].apply(lambda latitude: latitude_correction(latitude))

# Correct instances with abnormal longitude value
X_train['longitude'] = X_train['longitude'].apply(lambda longitude: longitude_correction(longitude))
X_test['longitude'] = X_test['longitude'].apply(lambda longitude: longitude_correction(longitude))

X_train, scaler = preprocess_data(X_train)
X_test, _ = preprocess_data(X_test, scaler)

# KERAS
X_train, X_validation, y_train, y_validation = train_test_split(X_train, y_train, test_size=0.1, random_state=0)

act_func = 'relu'

print('\nTraining with -->{0}<-- activation function\n'.format(act_func))
dims = y_train.shape[1]
model = neural_network(activation=act_func, X=X_train, n_outputs=dims)
model.summary()

result = model.fit(X_train, y_train,
                   batch_size=16,
                   epochs=100,
                   verbose=1,
                   validation_data=(X_validation, y_validation))

rmse_finale = plot_act_func_results(result, act_func)

risultati(act_func, rmse_finale)

y_test = model.predict(X_test)
print(y_test)

df_y_test = pd.DataFrame(y_test)
df_y_test.to_csv('y_test.csv')

K.clear_session()
del model
