import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from keras import *


def latitude_correction(latitude):
    if latitude >= 9000:
        latitude = latitude / 1000
    elif latitude >= 900:
        latitude = latitude / 100
    elif latitude >= 90:
        latitude = latitude / 10
    return latitude


def longitude_correction(longitude):
    if longitude <= -18000:
        longitude = longitude / 1000
    elif longitude <= -1800:
        longitude = longitude / 100
    elif longitude <= -180:
        longitude = longitude / 10
    return longitude


def preprocess_data(x, scaler=None):
    """Preprocess input data by standardise features
    by removing the mean and scaling to unit variance"""
    if not scaler:
        scaler = StandardScaler()
        scaler.fit(x)
    x = scaler.transform(x)
    return x, scaler


def neural_network(X, activation, n_outputs):
    inputs = Input(shape=(X.shape[1]))
    x = layers.Dense(64, activation=activation)(inputs)
    x = layers.Dense(64, activation=activation)(x)
    x = layers.Dense(n_outputs, activation=activation)(x)
    model = Model(inputs, x)
    model.compile(loss='mse', optimizer='adam', metrics=[tf.keras.metrics.RootMeanSquaredError()])
    return model


def plot_act_func_results(result, activation_functions):
    plt.figure(figsize=(10, 7))
    plt.style.use('dark_background')

    # Plot of the validation root mean squared error
    plt.figure(figsize=(10, 10))

    plt.plot(result.history['val_root_mean_squared_error'])
    rmse_finale = result.history['val_root_mean_squared_error'][-1]

    plt.title('Model RMSE')
    plt.ylabel('Validation RMSE')
    plt.xlabel('Epoch')
    plt.legend(activation_functions)
    plt.show()

    return rmse_finale


def risultati(activation_function, rmse):
    print('RMSE of the activation function {} is {}'.format(activation_function, round(rmse, 5)))
